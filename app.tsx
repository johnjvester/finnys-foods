/*
 * Copyright (c) 2020-present, salesforce.com, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 * following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { oauth, net } from 'react-native-force';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as farStar } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

interface Response {
    records: Meal[]
}

interface Meal {
    Id: String,
    Name: String,
    Rating__c: number
}

interface Props {
}
  
interface State {
    data : Meal[]
}

class MealListScreen extends React.Component<Props, State> {
    constructor(props:Props) {
        super(props);
        this.state = {data: []};
    }
    
    componentDidMount() {
        var that = this;
        oauth.getAuthCredentials(
            () => that.fetchData(), // already logged in
            () => {
                oauth.authenticate(
                    () => that.fetchData(),
                    (error) => console.log('Failed to authenticate:' + error)
                );
            });
    }

    fetchData() {
        var that = this;
        net.query('SELECT Id, Name, Rating__c FROM Meal__c ORDER BY Name ASC LIMIT 100',
                  (response:Response) => {
                that.setState({data: response.records});
            },
                  (error) => console.log('Failed to query:' + error)
                 );
    }

    render() {
        return (
            <View style={styles.container}>
              <FlatList
                data={this.state.data}
                renderItem={({item}) => {
                    return (
                        <Text style={styles.item}>{item.Name} {this.getStars(item.Rating__c)}</Text>
                    )
                } }
                keyExtractor={(item, index) => {
                    return item.Id.toString();
                }}
              />
            </View>
        );
    }

    private getStars(rating: number) {
        const FontAwesomeStar = <FontAwesomeIcon icon={ faStar } />;
        const FontAwesomeEmptyStar = <FontAwesomeIcon icon={ farStar } />;

        if (rating === 5) {
            return ([
                FontAwesomeStar, FontAwesomeStar, FontAwesomeStar, FontAwesomeStar, FontAwesomeStar
            ]);
        } else if (rating === 4) {
            return ([
                FontAwesomeStar, FontAwesomeStar, FontAwesomeStar, FontAwesomeStar, FontAwesomeEmptyStar
            ]);
        } else if (rating === 3) {
            return ([
                FontAwesomeStar, FontAwesomeStar, FontAwesomeStar, FontAwesomeEmptyStar ,FontAwesomeEmptyStar
            ]);
        } else if (rating === 2) {
            return ([
                FontAwesomeStar, FontAwesomeStar, FontAwesomeEmptyStar ,FontAwesomeEmptyStar ,FontAwesomeEmptyStar
            ]);
        }

        return ([
            FontAwesomeStar, FontAwesomeEmptyStar, FontAwesomeEmptyStar, FontAwesomeEmptyStar, FontAwesomeEmptyStar
        ]);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        backgroundColor: 'white',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    }
});

const Stack = createStackNavigator();

export const App = function() {
    return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Finny's Foods" component={MealListScreen} />
          </Stack.Navigator>
        </NavigationContainer>
    );
}
